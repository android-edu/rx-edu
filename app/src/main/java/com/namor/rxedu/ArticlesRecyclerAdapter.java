package com.namor.rxedu;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ArticlesRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ArticlesModel> articlesModels = new ArrayList<>();
    private Context context;

    private Listener listener;

    interface Listener {

        void onClick(int position);
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public ArticlesRecyclerAdapter(List<ArticlesModel> articlesModels, Context context) {
        this.articlesModels = articlesModels;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext()).inflate(R.layout.item_article, parent, false);

        return new NewsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        NewsViewHolder viewHolder = (NewsViewHolder) holder;
        viewHolder.title.setText(articlesModels.get(position).getName());

        Picasso.with(context)
                .load(articlesModels.get(position).getImageUrl())
                .into(viewHolder.image);

        viewHolder.cardView.setOnClickListener(view -> {
            if (listener != null)
                listener.onClick(position);
        });

    }

    @Override
    public int getItemCount() {
        return articlesModels.size();
    }

    public class NewsViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.title)
        public TextView title;

        @BindView(R.id.image)
        public ImageView image;

        @BindView(R.id.card_view)
        public CardView cardView;

        public NewsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
