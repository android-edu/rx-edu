package com.namor.rxedu;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;

import io.reactivex.Observable;

public class RepositoryImpl implements IRepository {
    @Override
    public Observable<ArrayList<ArticlesModel>> getArticles(String urlLink) {
        return Observable.create(observableEmitter -> {
            ArrayList<ArticlesModel> articlesModels = new ArrayList<>();
            Document doc;
            try {
                doc = Jsoup.connect(urlLink).get();

                Elements titleElement = doc.getElementsByClass("title");
                Elements imageElement = doc.getElementsByClass("cover-image");

                for (int i = 0; i < titleElement.size(); i++) {
                    Elements imgsrc = imageElement.get(i).select("img");
                    String style = imgsrc.attr("src");

                    Elements ahref = titleElement.get(i).select("a");
                    String titleText = ahref.text();
                    String url = ahref.attr("href");

                    ArticlesModel articlesModel = new ArticlesModel();
                    articlesModel.setName(titleText);
                    articlesModel.setImageUrl(style);
                    articlesModel.setArticleUrl(url);

                    articlesModels.add(articlesModel);
                }
                observableEmitter.onNext(articlesModels);
            } catch (IOException e) {
                observableEmitter.onError(e);
            } finally {
                observableEmitter.onComplete();
            }
        });
    }
}
