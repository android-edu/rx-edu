package com.namor.rxedu;

import java.util.ArrayList;

import io.reactivex.Observable;

public interface IRepository {
    Observable<ArrayList<ArticlesModel>> getArticles(String url);
}
